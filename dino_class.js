// CLASS DINO 

class Dino {
    /**
     * @param {Array} simpleAttack 
     * @param {Array} superAttack 
     * @param {HTMLElement} imagePerso
     * @param {HTMLElement} imageRival 
     */
    constructor(simpleAttack, superAttack, imagePerso, imageRival) {
        this.simpleAttack = simpleAttack;
        this.superAttack = superAttack;
        this.imagePerso = imagePerso;
        this.imageRival = imageRival;
    }
    /**
     * @param {HTMLBodyElement} power
     */
    // All dinos images are display none on style.css. So this function display 'block' them.
    initPerso(dino){ 
        if(persoChoice === dino && rivalChoice !== dino){
            this.imagePerso.style.display = 'block';
        }else if(rivalChoice === dino && persoChoice !== dino){
            this.imageRival.style.display = 'block';
        }else if(persoChoice === dino && rivalChoice === dino){
            this.imagePerso.style.display = 'block'; 
            this.imageRival.style.display = 'block';
        }
    }
    // attack() is a method to do an attack. Rival or Perso power value falls when this function is called / when players move
    attack(power){
        power.value -= this.simpleAttack[Math.floor(Math.random()*this.simpleAttack.length)];
        message(); // message() is in dino_game_functions.js
        displayNone(); // in the same file
    }
    // is a method to do a super attack
    maxiAttack(power){
        let fail = document.querySelector('.fail');
        let result = this.superAttack[Math.floor(Math.random()*this.superAttack.length)];
        power.value -= result; 

        if(result === 0){
            setTimeout(function(){fail.style.display = 'block'},100); // It's possible to failed a super attack
            setTimeout(function(){fail.style.display = 'none'}, 300);
        }
        message();
        displayNone();  
    }
    // For rival
    maxiAttackRival(power){
        let result = this.superAttack[Math.floor(Math.random()*this.superAttack.length)];
        power.value -= result; 

        message();
        displayNone();  
    }
    veloAttack(power){ // The first attack of velociraptor is a super attack(2500)
        power.value -= this.superAttack[0];
    }
}

// superAttackImg();