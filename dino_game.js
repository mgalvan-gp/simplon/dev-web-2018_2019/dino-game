// CREATE PERSO/RIVAL :

// Players instances
// The Dino class is in dino_class.js.
let tRex = new Dino([700, 800, 1000], [4000, 0, 0], document.querySelector('.tRex'), document.querySelector('.rivTrex'));
let velo = new Dino([250, 300, 400], [2500, 0, 0], document.querySelector('.velo'), document.querySelector('.rivVelo'));
let tri = new Dino([300, 450, 550], [3000, 0, 0], document.querySelector('.tri'), document.querySelector('.rivTri'));
let spin = new Dino([500, 600, 700], [3500, 0, 0], document.querySelector('.spin'), document.querySelector('.rivSpin'));
let anky = new Dino([350, 450, 600], [3200, 0, 0], document.querySelector('.anky'), document.querySelector('.rivAnky'));
let gira = new Dino([250, 350, 450], [3000, 0, 0], document.querySelector('.gira'), document.querySelector('.rivGira'));
let carno = new Dino([750, 750, 1050], [4000, 0, 0], document.querySelector('.carno'), document.querySelector('.rivCarno'));

// Init dinosTab to generate a rival or a perso randomly.
let dinosTab = [tRex, velo, tri, spin, anky, gira, carno];
// Rival & perso are init.
let rivalChoice = dinosTab[Math.floor(Math.random()*dinosTab.length)];
let persoChoice = dinosTab[Math.floor(Math.random()*dinosTab.length)];

// DINOS IMAGES :

// Function initPerso(), display 'block' the instances images with rivalChoice and persoChoice. 
// It is in another file, dino_game_functions.js.
tRex.initPerso(tRex);
velo.initPerso(velo);
tri.initPerso(tri);
spin.initPerso(spin);
anky.initPerso(anky);
gira.initPerso(gira);
carno.initPerso(carno);

// INIT VARIABLES TO PLAY :

// Your perso
let myPerso = document.querySelector('#myPerso'); // The left div
// Your rival
let myRival = document.querySelector('#myRival'); // The Rigth div

// Init variables power
let persoPower = document.querySelector('.persoPower'); // Power perso
let rivalPower = document.querySelector('.rivalPower'); // Power Rival

/* let shake = document.querySelector('.shake'); */

// SPECIAL VELOCIRAPTOR :

// Init the arrows images, to show that the velociraptor is faster.
let msgVelo = document.querySelector('.fasterImgPerso');
let msgVeloRival = document.querySelector('.fasterImgRiv');

// The Velociraptor is faster so, he's start first.
setTimeout(function(){veloMode()},100); // veloMode() is in dino_game_functions.js

         
// SPECIAL VELOCIRAPTOR END 

// START TO PLAY :

let bodyEl = document.querySelector('body'); 
// Event on keyup 
bodyEl.addEventListener('keyup', function(e){
    if(e.key === 'ArrowRight'){
        // This functions are in dino_game_functions.js
        persoMove(); 
        rivalMove();
        setTimeout(function(){
            if(persoChoice === tRex){
                tRex.attack(rivalPower);        // attack() in dino_class.js.
            } else if(persoChoice === velo){
                velo.attack(rivalPower);        // 
            } else if(persoChoice === tri){
                tri.attack(rivalPower);         //
            } else if(persoChoice === spin){
                spin.attack(rivalPower);        //
            } else if(persoChoice === anky){
                anky.attack(rivalPower);        //
            } else if(persoChoice === gira){
                gira.attack(rivalPower);        //
            }else if(persoChoice === carno){
                carno.attack(rivalPower);       //
            }
        }, 200);

        setTimeout(function(){
            if(rivalChoice === tRex){
                tRex.attack(persoPower);        //
            }else if(rivalChoice === velo){
                velo.attack(persoPower);        //
            }else if(rivalChoice === tri){
                tri.attack(persoPower);         //    
            }else if(rivalChoice === spin){
                spin.attack(persoPower);        //
            }else if(rivalChoice === anky){
                anky.attack(persoPower);        //
            }else if(rivalChoice === gira){
                gira.attack(persoPower);        //
            }else if(rivalChoice === carno){
                carno.attack(persoPower);       //
            }
        }, 900);
    }

        superAttackImg();
        if(persoPower.value < 15000 && persoPower.value > 12000 ){
            
            if(e.key === ' '){
                persoMove(); 
                rivalMove();
                setTimeout(function(){
                    if(persoChoice === tRex){
                        tRex.maxiAttack(rivalPower);        // attack() in dino_class.js.
                    } else if(persoChoice === velo){
                        velo.maxiAttack(rivalPower);        // 
                    } else if(persoChoice === tri){
                        tri.maxiAttack(rivalPower);         //
                    } else if(persoChoice === spin){
                        spin.maxiAttack(rivalPower);        //
                    } else if(persoChoice === anky){
                        anky.maxiAttack(rivalPower);        //
                    } else if(persoChoice === gira){
                        gira.maxiAttack(rivalPower);        //
                    } else if(persoChoice === carno){
                        carno.maxiAttack(rivalPower);       //
                    }
                }, 200); 
            console.log(persoPower);
            }
        

                setTimeout(function(){
                    if(rivalChoice === tRex){
                        tRex.maxiAttackRival(persoPower);        //
                    }else if(rivalChoice === velo){
                        velo.maxiAttackRival(persoPower);        //
                    }else if(rivalChoice === tri){
                        tri.maxiAttackRival(persoPower);         //    
                    }else if(rivalChoice === spin){
                        spin.maxiAttackRival(persoPower);        //
                    }else if(rivalChoice === anky){
                        anky.maxiAttackRival(persoPower);        //
                    }else if(rivalChoice === gira){
                        gira.maxiAttackRival(persoPower);        //
                    }else if(rivalChoice === carno){
                        carno.maxiAttackRival(persoPower);       //
                    }
                }, 900);
                
            if(persoPower.value <= 20000 && persoPower.value >= 15000 || persoPower.value <= 12000 && persoPower.value >= 0){

            setTimeout(function(){
                if(rivalChoice === tRex){
                    tRex.attack(persoPower);        //
                }else if(rivalChoice === velo){
                    velo.attack(persoPower);        //
                }else if(rivalChoice === tri){
                    tri.attack(persoPower);         //    
                }else if(rivalChoice === spin){
                    spin.attack(persoPower);        //
                }else if(rivalChoice === anky){
                    anky.attack(persoPower);        //
                }else if(rivalChoice === gira){
                    gira.attack(persoPower);        //
                }else if(rivalChoice === carno){
                    carno.attack(persoPower);       //
                }
            }, 900);
        }
    }
})

// PLAY END 




